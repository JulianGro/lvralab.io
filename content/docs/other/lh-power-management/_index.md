---
weight: 300
title: Lighthouse Power Management
---

# Lighthouse Power Management

Turning your Base Stations on and off can be a little annoying, as you may have to plug and unplug them. So here's a few ways you can make it easier.

**Note:** Power management of Index (V2) Base Stations should work in SteamVR, but V1 doesn't.

You'll need a device with Bluetooth LE to use these: (You can use a dongle)

- [lighthouse_pm](https://github.com/jeroen1602/lighthouse_pm)
    - A Flutter app that supports V1 and V2 lighthouses, works best on android.
- [vr-lighthouse](https://github.com/ShayBox/Lighthouse)
    - Made in Rust for Linux, Windows and MacOS. Supports V1 and V2.

*Use them at your own risk, as there IS a possibility they could brick your base stations, however very* ***unlikely***.

Alternatively, use Google Home/smart power bars to turn on and off outlets used by your base stations.