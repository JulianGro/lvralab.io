---
weight: 900
title: Unity
---

# Unity

This article will take you through installing Unity Hub, Unity, and ALCOM (the FOSS alternative to VRChat Creator Companion).

## Set up UnityHub

Download the [UnityHub AppImage](https://public-cdn.cloud.unity3d.com/hub/prod/UnityHub.AppImage). Run the AppImage like you normally would.

Running UnityHub from [Flatpak](https://flathub.org/apps/com.unity.UnityHub) is also still an option, but it seems ALCOM has better integration with the AppImage.

First, take a look at this page from [the official VRChat docs](https://creators.vrchat.com/sdk/upgrade/unity-2022/#using-the-unity-hub) about how to install the supported Unity version from UnityHub. We'll do something similar.

1. Open Unity Hub and sign in with your Unity account.
1. Install **2022.3.22** (VRChat's current supported Unity version, at the time of writing) by running `./UnityHub.AppImage unityhub://2022.3.22f1/887be4894c44`.
1. When prompted, select *Android Build Support* and *Windows Build Support (Mono)*.

## ALCOM

vrc-get is a native FOSS package manager written in Rust. Its GUI counterpart was formerly known as vrc-get-gui, but has been rebranded as ALCOM (***Al***ternative Creator ***Com***panion).

1. Download the [AppImage from the latest "gui" release](https://github.com/vrc-get/vrc-get/releases). Pick the file named `alcom-<some version>-x86_64.AppImage`.
1. Run the AppImage.
1. In the settings tab, under Unity Hub Path, point it to your UnityHub.AppImage.
1. Create a new project and open it.
1. Go to VRChat SDK -> Show Control Panel and dock it to the main window.
1. It may open windows partially offscreen. If this happens, see [this section below](#editor-windows-appear-partially-offscreen).

Now you should be able to create and upload avatars and worlds like you normally would.

<!--
This is no longer maintained.

## Option 2. LinuxCreatorCompanion

If you need some of the GUIs, it is now possible to use VRChat Creator Companion (VCC) on Linux.

The official VRChat Creator Companion uses Webview2 (Microsoft Edge). This component is difficult to install, and doesn't run natively. Instead, you can use LinuxCreatorCompanion.

[GitHub repo for LinuxCreatorCompanion](https://github.com/RinLovesYou/LinuxCreatorCompanion)

It works by using wine to extract the necessary bits to re-embed into a native client application. While experimental, you can enjoy much easier avatar, world, and asset creation. Again, this is experimental, and we discourage you from trying this on important projects. **Remember to make frequent project backups.** Please leave any feedback about bugs or ideas on the GitHub issues tab.

To install, click the releases tab, download the zip, extract it somewhere, and run `./LinuxCreatorCompanion`.

**Note:** If you're using Unity Hub via flatpak, it will have trouble finding and opening Unity. For now, create this wrapper script, chmod +x it, and put it somewhere in your PATH, like `/usr/local/bin/unityhub`.

```bash
#!/usr/bin/env bash
exec flatpak run com.unity.UnityHub "$@"
```

-->

## Troubleshooting

### Worlds: AssetBundle was not built

This error is caused by VRCSDK build incorrectly expecting a mixed-case AssetBundle filename, while Unity outputs a lowercase filename, which fails a file existence check and causes the SDK to abort the build.

To fix this, import `WorldSDKPatcher.unitypackage`: https://github.com/thegu5/VRCSDKonLinux/releases

### Shaders fail to include some .cginc files

This can happen with Poiyomi. It's caused by case-sensitivity. You may need to edit the shader files directly. Open them in any text editor, and correct the casing of filenames.

### Editor windows appear partially offscreen

Unity may open undocked panel windows (such as color pickers and unitypackage import dialogs) partially offscreen.

Some workarounds:
- For KDE, hold the Meta key and click anywhere in the window to drag it back onscreen.
- For KDE, press ALT+F3 -> More Actions -> Move.

### Drag-and-drop does not work

Rather than using Assets -> Import New Asset or Import Package, it's normally possible to drag-and-drop a unitypackage or other asset from a file browser directly into Unity's Project pane, where it will be copied to the Assets folder.

However, if your file browser is running on Wayland, this will show "Failed copying file" errors.

See issue [flathub/com.unity.UnityHub#97](https://github.com/flathub/com.unity.UnityHub/issues/97).

As a workaround, launch your file browser in Xwayland mode. Unity will accept the file as it should.

```
WAYLAND_DISPLAY="" dolphin
```
