---
weight: 50
title: Hardware
---

# Hardware



## GPU support matrix

| Manufacturer | Driver                    | VR Support     | Reprojection Support           | Hybrid Graphics Support       | Notes                                                                                        |
|--------------|---------------------------|----------------|--------------------------------|-------------------------------|----------------------------------------------------------------------------------------------|
| Nvidia       | Nvidia (Closed Source)    | Excellent      | Excellent                      | Supported                     | Requires an implicit [vulkan-layer](https://gitlab.freedesktop.org/monado/utilities/vulkan-layers) [*AUR package*](https://aur.archlinux.org/packages/monado-vulkan-layers-git) to not segfault. |
| Nvidia       | Nouveau (Open Source)     | Functional     | Functional                     | Supported                     | Lacks DisplayPort audio.                                                  |
| Intel        | i915 (Open Source)        | Functional     | Unknown                        | Supported                     | Relatively old, most likely poor support for newer GPUs.
| Intel        | Intel/Xe (Open Source)    | No wired HMDs  | N/A / Unable to drive wired.   | Supported                     | Lacks direct mode implementation in driver, unable to drive wired HMDs.   |
| AMD          | RADV (Open Source)        | Excellent      | Robust (RDNA+)                 | Supported                     | RDNA generation and up supported with compute tunneling for reprojection. Lower than RDNA are not robust. |
| AMD          | AMDVLK (Open Source)      | Use RADV       | Use RADV                       | N/A                           | RADV preferred in all circumstances, unable to drive wired HMDs. Do not use. Do not seek support. |
| AMD          | AMDGPU PRO (Closed Source)| Use RADV       | Use RADV                       | N/A                           | RADV preferred in all circumstances, unable to drive wired HMDs. Do not use. Do not seek support. |

**Notes:**
- **VR Support**: Indicates how well supported the necessary Vulkan API components are.
- **Reprojection Support**: Describes the support and quality of reprojection features for VR. Poor support indicates that the driver is not able to properly handle Vulkan realtime shaders and it will present as visual stutter. Non-robust solutions will suffer stutter under very high GPU load.
- **PRIME/ Hybrid GPU Support**: Compatibility with systems using multiple GPUs for render offload. Monado and all clients must be run on a single select GPU due to memory tiling requirements.
- For Nvidia proprietary drivers, the [vulkan-layer](https://gitlab.freedesktop.org/monado/utilities/vulkan-layers) is **required** not to crash.
- AMD GPUs lower than RDNA generation have functional but less robust reprojection capabilities, expected to be similar to Intel.
- Audio over displayport is known to temporarily cut out when new audio sources spring up on pipewire [without a fix to add alsa headroom](https://wiki.archlinux.org/title/PipeWire#Audio_cutting_out_when_multiple_streams_start_playing)
- X11 configurations are discouraged but workable, please upgrade your system to Wayland if at all possible.



## XR Devices

A non-comprehensive table of various VR/XR devices and the drivers that support them.

| Device               | [SteamVR](/docs/steamvr/)             | [Monado](/docs/fossvr/monado/) | [WiVRn](/docs/fossvr/wivrn/) |
|----------------------|:-------------------------------------:|:------------------------------:|:----------------------------:|
| **PC VR**            |                                       |                                |                              |
| Valve Index          | ✅                                    | ✅                             | 🚧 (WiVRn PC-PC stream)      |
| HTC Vive             | ✅                                    | ✅                             | 🚧 (WiVRn PC-PC stream)      |
| HTC Vive Pro         | ✅                                    | ✅                             | 🚧 (WiVRn PC-PC stream)      |
| HTC Vive Pro Eye     | ✅ (No eyechip)                       | ✅ (No eyechip)                | 🚧 (WiVRn PC-PC stream)      |
| HTC Vive Pro 2       | ✅ (custom [driver and patches](https://github.com/CertainLach/VivePro2-Linux-Driver)) | ? (presumably with two kernel patches [1](https://github.com/CertainLach/VivePro2-Linux-Driver/blob/master/kernel-patches/0002-drm-edid-parse-DRM-VESA-dsc-bpp-target.patch) [2](https://github.com/CertainLach/VivePro2-Linux-Driver/blob/master/kernel-patches/0003-drm-amd-use-fixed-dsc-bits-per-pixel-from-edid.patch)) | --                           |
| Bigscreen Beyond     | ~ (Functional after monado has run. Doesn't work on Nvidia)   | ✅ (with [kernel patches](https://gist.githubusercontent.com/TayouVR/af8635a4b8e1d02d038be1be1d221c83/raw/3806a6ff0a03721904164277d7523d43f7ca383c/bigscreenBeyond.patch). Doesn't work on Nvidia)                   | --                           |
| Somnium VR1          | ?                                     | ?                              | ?                            |
| VRgineers XTAL       | ?                                     | ?                              | ?                            |
| StarVR One           | ?                                     | ?                              | ?                            |
| Varjo VR-1           | ?                                     | ?                              | ?                            |
| Varjo VR-2           | ?                                     | ?                              | ?                            |
| Varjo VR-3           | ?                                     | ?                              | ?                            |
| Pimax 4K             | ❌ (Planned)                          | 🚧 (WIP, with [kernel patches](https://gist.githubusercontent.com/TayouVR/60e3ee5f95375827a66a8898bea02bec/raw/c85135c8d8821ebb2fa85629d837a41de57e12ef/pimax.patch)) | 🚧 (WiVRn PC-PC stream)          |
| Pimax 5K Plus        | ❌ (Planned)                          | 🚧 (WIP, with [kernel patches](https://gist.githubusercontent.com/TayouVR/60e3ee5f95375827a66a8898bea02bec/raw/c85135c8d8821ebb2fa85629d837a41de57e12ef/pimax.patch)) | 🚧 (WiVRn PC-PC stream)          |
| Pimax 5K XR          | ❌ (Planned)                          | 🚧 (WIP, with [kernel patches](https://gist.githubusercontent.com/TayouVR/60e3ee5f95375827a66a8898bea02bec/raw/c85135c8d8821ebb2fa85629d837a41de57e12ef/pimax.patch)) | 🚧 (WiVRn PC-PC stream)          |
| Pimax 5K SUPER       | ❌ (Planned)                          | 🚧 (WIP, with [kernel patches](https://gist.githubusercontent.com/TayouVR/60e3ee5f95375827a66a8898bea02bec/raw/c85135c8d8821ebb2fa85629d837a41de57e12ef/pimax.patch)) | 🚧 (WiVRn PC-PC stream)          |
| Pimax 8K             | ❌ (Planned)                          | 🚧 (WIP, with [kernel patches](https://gist.githubusercontent.com/TayouVR/60e3ee5f95375827a66a8898bea02bec/raw/c85135c8d8821ebb2fa85629d837a41de57e12ef/pimax.patch)) | 🚧 (WiVRn PC-PC stream)          |
| Pimax Vision 8K X    | ❌ (Planned)                          | 🚧 (WIP, with [kernel patches](https://gist.githubusercontent.com/TayouVR/60e3ee5f95375827a66a8898bea02bec/raw/c85135c8d8821ebb2fa85629d837a41de57e12ef/pimax.patch)) | 🚧 (WiVRn PC-PC stream)          |
| Pimax Vision 8K PLUS | ❌ (Planned)                          | 🚧 (WIP, with [kernel patches](https://gist.githubusercontent.com/TayouVR/60e3ee5f95375827a66a8898bea02bec/raw/c85135c8d8821ebb2fa85629d837a41de57e12ef/pimax.patch)) | 🚧 (WiVRn PC-PC stream)          |
| Lenovo Explorer      | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| Acer AH101           | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| Dell Visor           | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| HP WMR headset       | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| Samsung Odyssey      | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| Asus HC102           | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| Samsung Odyssey+     | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| HP Reverb            | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| Acer OJO 500         | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| HP Reverb G2         | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| Oculus Rift CV1      | 🚧 (Monado SteamVR plugin)            | 🚧 (ancient openhmd based support) | 🚧 (WiVRn PC-PC stream)      |
| Oculus Rift S        | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| **Standalone**       |                                       |                                |                              |
| Quest                | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Quest 2              | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Quest Pro            | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Quest 3              | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Pico 4               | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Pico Neo 3           | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| HTC Vive Focus 3     | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| HTC Vive XR Elite    | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Lynx R1              | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Apple Vision Pro     | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ❌                           |
| **Trackers**         |                                       |                                |                              |
| Vive/Tundra trackers | ✅                                    | ✅                             | ❌                           |
| SlimeVR trackers     | ✅                                    | ✅ (OSC, driver WIP)           | 🚧 (Proposed)                |
| Project Babble       | ✅ (oscavmgr)                         | ✅ (oscavmgr)                  | ✅ (oscavmgr)                |
| Eyetrack VR          | ✅                                    | ✅                             | ✅                           |
| Mercury Handtrack    | 🚧 (Monado SteamVR plugin, win only)  | ✅ (survive driver only)       | ❌                           |
| Lucid VR Gloves      | ?                                     | ✅ (survive driver only)       | ❌                           |
| Standable FBT        | ❌                                    | ❌                             | ❌                           |

## Hardware note

WiVRn PC XR to WiVRn PC client streaming remains mainly for debug use.

Vive Pro microphones should be set to use 44.1khz sample rates as feeding in 48khz raises the pitch of the audio.

Valve index audio output should be set to 48khz or no audio will output.

Vive Pro Eye HMD functional, eyechip WIP.

Pimax initialization code WIP.

Eyetrack VR and Project Babble will both be implemented through [oscavmgr](https://github.com/galister/oscavmgr) to emit proper unified flexes over OSC.

## Applying a kernel patch (for Vive Pro 2, Bigscreen Beyond)

### Arch

1. download the patch(es), then follow the steps on https://wiki.archlinux.org/title/Kernel/Arch_build_system,
    which should be roughly: `mkdir ~/build/` -> `cd ~/build/` -> `pkgctl repo clone --protocol=https linux` -> `cd linux`
2. (section 2 of link) open `PKGBUILD` in that directory and modify the `pkgbase` line (at the top) to read e.g. "`pkgbase=linux-customvr`". then add a line containing "`<patchfilename>.patch`" to the `source` array:

```pkgbuild
source=(
  https://cdn.kernel.org/pub/linux/kernel/v${pkgver%%.*}.x/${_srcname}.tar.{xz,sign}
  <patchfilename>.patch
  $url/releases/download/$_srctag/linux-$_srctag.patch.zst{,.sig}
  config  # the main kernel config file
)
```

4. close `PKGBUILD`. (may not be necessary but i ran `updpkgsums`) -> `makepkg -s --skippgpcheck` (i didn't feel like fixing a pgp error)
5. then section 4 of https://wiki.archlinux.org/title/Kernel/Arch_build_system#Installing and restart

### NixOS

1. download the patch(es)
2. add this to your configuration.nix or other nix file you use:
```nix
  boot.kernelPatches = [
    {
      name = "type what the patch is for here";
      patch = /path/to/patch/file.patch;
    }
  ];
```
reference commit on my nix files: https://github.com/TayouVR/nixfiles/commit/d6ef568a2642c5a26eb92611be7335afdb6047de