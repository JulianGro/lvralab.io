---
title: EyeTrackVR
weight: 250
---

# EyeTrackVR

DIY eye tracking for any headset. Here are the steps to get the software working on linux.

## EyeTrackApp

`git clone https://github.com/EyeTrackVR/EyeTrackVR.git`

`cd EyeTrackVR`

`git checkout v2.0-beta-feature-branch`

`poetry install --no-root`

`cd EyeTrackApp`

and bada bing bada boom you now should now have a fully functional eyetrack app when you run:

`poetry run python3 eyetrackapp.py`

## VRChat Native Eyetracking

VRCFT support will be added soon in oscavmgr. for now vrc native eyetracking works well. If you're having issues with your avatar cocnstantly squinting like I was, you can make a little change to osc.py at line 239 to correct that to your liking.

```diff
     if self.config.gui_vrc_native:  # VRC NATIVE

+        eye_blink = min(1.0, eye_blink * eyeblink_offset) # helps keep eyes open in VRC native eyetracking

         if self.main_config.eye_display_id in [
             EyeId.RIGHT,
```
This should only affect native eyetracking. replace `eyeblink_offset` with your preferred multiplier. I set mine to `1.8`

Squinting issues may also be caused by using IBO, so you may want to turn off "Intensity Based Openness" in blink algo settings.